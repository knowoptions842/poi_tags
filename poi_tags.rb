# TODO
#
unless ARGV.length == 2
  STDERR.puts "Run like: ruby poi_tags.rb ~/Downloads/directory_of_json_files fortresses"
  exit 1
end

file_or_directory = ARGV.first
raise "file/directory doesn't exist" unless File.exists?(file_or_directory)

VALID_POI_TYPES = {
  "f" => "fortresses",
  "g" => "greenhouses",
  "i" => "inns",
  "n" => "nothpwu"
}

poi_type = VALID_POI_TYPES[ARGV[1].to_s.downcase[0]]
unless poi_type
  STDERR.puts "Invalid poi, type: #{ARGV[1]}, try one of #{VALID_POI_TYPES.keys + VALID_POI_TYPES.values}"
  exit 1
end

PRINT_LIMIT            = 50
TARGET_DATE            = "2019-03-12T00:00:00Z" # Set to nil to get current OSM data
WAIT_TIME              = 6
WAIT_TIME_ON_EXCEPTION = 30

DO_ENCLOSING_TAGS = true
DO_NEARBY_TAGS = true

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'overpass-api-ruby'
  gem 'pry-byebug'
end

require 'csv'
require 'overpass_api_ruby'
require 'pathname'
require 'pp'

# From https://en.wikipedia.org/wiki/Decimal_degrees
# decimal degrees   unambiguously recognizable objs at this scale   N/S or E/W at equator   E/W at 23N/S   E/W at 45N/S   E/W at 67N/S
# 0.001             neighborhood, street                            111.32 m                102.47 m       78.71 m        43.496 m
# 0.0001            individual street, land parcel                  11.132 m                10.247 m       7.871 m        4.3496 m
# 0.00001           individual trees, door entrance                 1.1132 m                1.0247 m       787.1 mm       434.96 mm
#
# This value is:
#  * added to the latitude of a POI to get the northern point
#  * subtracted from the latitude of a POI to get the southern point
#  * added to the longitude of a POI to get the eastern point
#  * subtracted from the longitude of a POI to get the western point
#
# The most populous latitude is 25-26 degrees North latitude and 77-78 degrees East Longitude,
# so we'll assume that decimal degress 0.0001 == 10.247m.
NEARBY_TAG_METER_RADIUS = 25.0

# List of endpoints to use randomly to avoid overwhelming any single instance:
# https://wiki.openstreetmap.org/wiki/Overpass_API
ENDPOINTS = %w{
  https://lz4.overpass-api.de/api/interpreter
  https://z.overpass-api.de/api/interpreter
  https://overpass.kumi.systems/api/interpreter
  https://overpass.nchc.org.tw/api/interpreter
}

EXCLUDED_TAGS = %w{
  addr
  alt_name
  attribution
  contact
  description
  default_language
  email
  fax
  flag
  int_name
  name
  network
  nist
  note
  official_name
  old_name
  operator
  population
  phone
  short_name
  sorting_name
  source
  tiger
  timezone
  TMC
  website
  wikidata
  wikipedia
}

EXCLUDED_TAGS_REGEX = /^(#{EXCLUDED_TAGS.join("|")})/

PORTAL_NAMES_TO_DO = [
]

# Doesn't support attic/history: http://overpass.openstreetmap.fr/api/interpreter

module OverpassAPI
  # builds queries in overpass ql format
  class QL < Base
    def initialize(args = {})
      super
      @maxsize = args[:maxsize]
      @date    = args[:date]
    end

    def build_query(query)
      header = ''
      header << "[bbox:#{@bbox}]" if @bbox
      header << "[timeout:#{@timeout}]" if @timeout
      header << "[maxsize:#{@maxsize}]" if @maxsize
      header << "[date:'#{@date}']"     if @date

      header << '[out:json]'

      "#{header};#{query}".tap {|string| puts "query: #{string}"}
    end
  end
end

def nearby_ways_nodes_tags_tags_query(south, west, north, east)
  # Taken from:
  # https://wiki.openstreetmap.org/wiki/Overpass_API/Advanced_examples#Completed_ways.2C_but_not_relations
  # This call includes all nodes in the bounding box, all ways in the bounding
  # box, all member nodes of these ways whether these are inside the bounding
  # box or not, and all relations that have such a node or such a way as
  # members. For the moment, the call of way with a bounding box makes the query
  # sometimes slow. I will work on it, but please be patient for the moment.
  # Please observe that not all returned relations are displayed in Open Layers
  # because Open Layers requires all nodes of a way to be present.
  <<~QUERYHEREDOC
    (
      node(#{south},#{west},#{north},#{east});
      rel(bn)->.x;
      way(#{south},#{west},#{north},#{east});
      node(w)->.x;
      rel(bw);
    );
    out meta;
  QUERYHEREDOC
end

def enclosing_ways_tags_query(latitude, longitude, south, west, north, east)
# /* Ringwood state park: "enclosing features:
# is_in
# */
# [bbox:41.119804092319704,-74.2747779076803,41.158839907680296,-74.23574209231971][date:'2019-03-12T00:00:00Z'][out:json];is_in(41.139322,-74.25526)->.a;
# way(pivot.a);
# out tags bb;
# out ids geom(41.119804092319704,-74.2747779076803,41.158839907680296,-74.23574209231971);
# relation[admin_level!=1][admin_level!=2][admin_level!=3][admin_level!=4][admin_level!=5][admin_level!=6][admin_level!=7][admin_level!=8](pivot.a);
# out tags bb;
  <<~QUERYHEREDOC
    is_in(#{latitude},#{longitude})->.a;
    way(pivot.a);
    out tags bb;
    out ids geom(#{south},#{west},#{north},#{east});
    relation[admin_level!=1][admin_level!=2][admin_level!=3][admin_level!=4][admin_level!=5][admin_level!=6][admin_level!=7][admin_level!=8](pivot.a);
    out tags bb;
  QUERYHEREDOC
end

def nearby_way_nodes_tags(latitude, longitude)
# /*
# // nearby
# [timeout:10][out:json];
# (
#   node(around:0.0001,42.458134,-71.460529);
#   way(around:0.0001,42.458134,-71.460529);
# );
# out tags geom(42.458034,-71.460629,42.458234000000004,-71.46042899999999);
# relation(around:0.0001,42.458134,-71.460529);
# out geom(42.458034,-71.460629,42.458234000000004,-71.46042899999999);
# */

end

# Northen hemisphere is the most populous and the highest latitude is 25-26 degrees North latitude and 77-78 degrees East Longitude, so we'll assume
# that decimal degress 0.0001 == 10.247m
def meters_to_decimal_degress(meters)
  meters / 102_470.0
end

def bounding_box(latitude, longitude, meter_radius)
  south = latitude - (meters_to_decimal_degress(meter_radius))
  west  = longitude - (meters_to_decimal_degress(meter_radius))
  north = latitude + (meters_to_decimal_degress(meter_radius))
  east  = longitude + (meters_to_decimal_degress(meter_radius))
  return south, west, north, east
end

def tags_for_location(latitude, longitude)
  tags1 = Set.new
  if DO_ENCLOSING_TAGS
    # Get enclosing way tags
    south, west, north, east = bounding_box(latitude, longitude, 10_000)
    query1 = enclosing_ways_tags_query(latitude, longitude, south, west, north, east)
    tags1 = run_query_for_tags(query1, south, west, north, east)
  end

  tags2 = Set.new
  if DO_NEARBY_TAGS
    # Get nearby tags
    south, west, north, east = bounding_box(latitude, longitude, NEARBY_TAG_METER_RADIUS)
    query2 = nearby_ways_nodes_tags_tags_query(south, west, north, east)
    tags2 = run_query_for_tags(query2, south, west, north, east)
  end

  tags = tags1 + tags2
  puts "tags found: #{tags.length}"
  tags << "_NO_TAGS_FOUND_" if tags.empty?
  tags
end

def run_query_for_tags(query, south, west, north, east)
  begin
    options=
    { :bbox =>
      {
        :s => south,
        :n => north,
        :w => west,
        :e => east,
        :timeout => 900,
        :maxsize => 1073741824
      },
      :endpoint => ENDPOINTS.rotate!.first,
      :date     => TARGET_DATE
    }

    overpass = OverpassAPI::QL.new(options)

    response = overpass.query(query)
  rescue => err
    puts "Waiting due to exception...#{err}"
    sleep WAIT_TIME_ON_EXCEPTION
    puts "Waiting due to exception...RETRYING"
    retry
  ensure
    puts "Waiting..."
    sleep WAIT_TIME
    puts "Waiting...DONE"
  end

  # Use a Set so we get only highway=footway
  # once per POI regardless of how many foot paths are near it.
  tags = Set.new
  response[:elements].each do |e|
    next unless e.key?(:tags)

    e[:tags].each do |k, v|
      next if k.to_s.match(EXCLUDED_TAGS_REGEX)
      tags << "#{k}=#{v}"
    end
  end
  tags
end

def intel_link(lat, lng)
  "https://intel.ingress.com/?pll=#{lat},#{lng}"
end

def osm_link(lat, lng)
  "https://www.openstreetmap.org/?mlat=#{lat}&mlon=#{lng}"
end

def print_totals(totals)
  totals.sort_by {|k, v| -v}.take(PRINT_LIMIT).each {|k, v| puts "#{v}: #{k}"}
end

files = Set.new
if File.file?(file_or_directory)
  files << file_or_directory
elsif File.directory?(file_or_directory)
  files = Dir.glob(Pathname.new(file_or_directory).join("*.json"))
end

csv_file = Pathname.new(__dir__).join("poi_tags_#{poi_type}.csv")
completed_coordinates = {}
totals = Hash.new(0)

headers = %w{ name latitude longitude type intel_link osm_link tags }

# Skip any POI with a common tag  For example, if you know leisure=park is used for fortresses
# and want to see what are the tags for fortresses without leisure=park, you can add
# leisure=park to this set below to skip any POI with that tag.
SKIPPED_POI_BY_TAG = Set.new(%w{})
skipped = Set.new
if File.exists?(csv_file)
  CSV.foreach(csv_file, :force_quotes => true, :headers => headers) do |row|
    next if row[headers.first] == headers.first

    tags = row[6..-1].compact
    if tags.to_set.intersect?(SKIPPED_POI_BY_TAG)
      skipped << "#{row[1]}_#{row[2]}"
      next
    end

    completed_coordinates["#{row[1]}_#{row[2]}"] = tags
    tags.each { |t| totals[t] += 1 }
  end
end

CSV.open(csv_file, "a+", :force_quotes => true, :headers => headers) do |csv|
  row = csv.shift
  csv << headers if row.nil? || row[headers.first] != headers.first

  total_poi_completed = completed_coordinates.keys.length
  puts "Starting #{poi_type}..."
  files.each do |file|
    puts "Starting #{file}..."
    h = JSON.parse(File.read(file))
    counter = 0
    h[poi_type].each do |_guid, hsh|
      coordinate = "#{hsh['lat']}_#{hsh['lng']}"
      next if skipped.include?(coordinate)
      next if (!PORTAL_NAMES_TO_DO.empty? && !PORTAL_NAMES_TO_DO.include?(hsh["name"]))
      puts "processing... #{hsh['name']}"

      new_to_csv = true
      tags = []

      if completed_coordinates[coordinate].to_a.empty?
        tags = tags_for_location(hsh["lat"], hsh["lng"]).sort_by(&:length)
        completed_coordinates[coordinate] = tags
        tags.each { |t| totals[t] += 1 }
        total_poi_completed += 1
      else
        new_to_csv = false
        puts "skipping: #{coordinate}, already processed"
      end

      if new_to_csv
        csv << [hsh["name"], hsh["lat"], hsh["lng"], poi_type, intel_link(hsh['lat'], hsh['lng']), osm_link(hsh['lat'], hsh['lng']), *tags]
        csv.flush
      end

      counter += 1
      if counter % 5 == 0
        puts "#{poi_type}: #{counter}/#{h[poi_type].length}. Total poi completed: #{total_poi_completed}"
        puts
        print_totals(totals)
      end
    end
    puts "#{poi_type}: Finished #{counter}/#{h[poi_type].length}.  Total poi completed: #{total_poi_completed}"
  end
  print_totals(totals)
end
