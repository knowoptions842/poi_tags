require "S2Geometry"

module S2CellExtension
  module VertexCoordinateCalculator
    import 'com.google.common.geometry'

    Location = Struct.new(:latitude, :longitude)

    def self.vertices_of_s2_cell(s2_level, latitude, longitude)
      cell = S2Cell.build_from_lat_long(latitude, longitude)
      level_x_parent = cell.parent(s2_level)
      [0, 1, 2, 3].collect do |n|
        s2_lat_lng = S2LatLng.new(level_x_parent.get_vertex(n))
        loc = Location.new(s2_lat_lng.latDegrees, s2_lat_lng.lngDegrees)
      end
    end
  end
end

latitude   = ARGV[0].to_f
longitude  = ARGV[1].to_f
cell_level = ARGV[2].to_i
puts S2CellExtension::VertexCoordinateCalculator.vertices_of_s2_cell(cell_level, latitude, longitude)
